const { StatusCodes } = require('http-status-codes')

function errorHandlerMiddleware(err, req, res, next) {
    let customError = {
        statusCode: err.statusCode || StatusCodes.INTERNAL_SERVER_ERROR,
        msg: err.message || 'Something went wrong try again later',
    }

    return res.status(customError.statusCode).json({ success: false, error: customError.msg })
}

module.exports = errorHandlerMiddleware
