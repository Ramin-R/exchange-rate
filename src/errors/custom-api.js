class CustomAPIError extends Error {
    constructor(message, statusCode = undefined) {
        super(message)
        if (statusCode) {
            this.statusCode = statusCode
        }
    }
}

module.exports = CustomAPIError
