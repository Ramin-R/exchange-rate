require('dotenv').config()
const http = require('http')
const mongoose = require('mongoose')
const app = require('./app')

const PORT = Number(process.env.PORT) || 8000
const server = http.createServer(app)

async function startServer() {
    await mongoose.connect(process.env.MONGO_URL)

    server.listen(PORT, () => {
        console.log(`listening on port ${PORT}`)
    })
}

startServer()