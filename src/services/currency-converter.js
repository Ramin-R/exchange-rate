const axios = require('axios')
const { StatusCodes } = require('http-status-codes')
const { CustomAPIError } = require('../errors')
const url = 'https://free.currconv.com/api/v7/convert'

async function getConvertedAmount(base, out, amount) {
    const pair = `${base}_${out}`
   try {
       const result = await axios.get(url, {
           params: {
               q: pair,
               compact: 'ultra',
               apiKey: process.env.API_KEY,
           }
       })

       if (!result.data[pair]) {
           return -1
       }

       return result.data[pair] * amount
   } catch (e) {
        const msg = (e.response && e.response.data && e.response.data.error) || 'Internal server error'
        const status = (e.response && e.response.data && e.response.data.status) || StatusCodes.INTERNAL_SERVER_ERROR
        throw new CustomAPIError(msg, status)
   }
}

module.exports = {
    getConvertedAmount,
}