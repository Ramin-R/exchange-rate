require('express-async-errors')
const express = require('express')
const api = require('./routes/api')
const errorHandlerMiddleware = require('./middlewares/error-handler')

const app = express()

app.use(express.json())
app.use('/v1', api)

app.use(errorHandlerMiddleware)

module.exports = app
