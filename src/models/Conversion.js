const mongoose = require('mongoose')

const ConversionSchema = new mongoose.Schema({
    base: {
        type: String,
        required: [true, 'Please provide base'],
        maxlength: 3,
        minlength: 3,
    },
    out: {
        type: String,
        required: [true, 'Please provide out'],
        maxlength: 3,
        minlength: 3,
    },
    amount: {
        type: Number,
        required: [true, 'Please provide amount'],
    },
    convertedAmount: {
        type: Number,
        required: [true, 'Please provide converted amount'],
    },
})

module.exports = mongoose.model('Conversion', ConversionSchema)
