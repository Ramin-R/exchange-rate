const express = require('express')
const { httpGetConvertedAmount } = require('./conversions.controller')

const conversionsRouter = express.Router()

conversionsRouter.get('/', httpGetConvertedAmount)

module.exports = conversionsRouter
