const { getConvertedAmount } = require('../../services/currency-converter')
const { BadRequestError } = require('../../errors')
const { Conversion, User } = require('../../models')

async function httpGetConvertedAmount(req, res) {
    let { base, out, amount } = req.query

    if (!amount) {
        amount = 1
    } else {
        amount = Number.parseFloat(amount)
        if (isNaN(amount)) {
            throw new BadRequestError('Amount is not a valid number')
        }
    }
    const convertedAmount = await getConvertedAmount(base, out, amount)

    if (convertedAmount === -1) {
        throw new BadRequestError('Invalid pair')
    }

    await User.updateOne(
        { _id: req.user.userId },
        { $push: { conversions: await Conversion.create({ base, out, amount, convertedAmount }) } }
    )

    return res.json({
        success: true,
        data: {
            convertedAmount,
        }
    })
}

module.exports = {
    httpGetConvertedAmount,
}