const express = require('express')
const conversionsRouter = require('./conversions/conversions.router')
const authRouter = require('./auth/auth.router')
const usersRouter = require('./users/users.router')
const authMiddleware = require('../middlewares/authentication')
const api = express.Router()

api.use('/', authRouter)
api.use('/conversions', authMiddleware, conversionsRouter)
api.use('/users', authMiddleware, usersRouter)

module.exports = api