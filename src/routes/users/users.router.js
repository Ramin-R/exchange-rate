const express = require('express')
const { httpGetAuthUserHistory, httpGetUsers } = require('./users.controller')

const usersRouter = express.Router()

usersRouter.get('/me', httpGetAuthUserHistory)
usersRouter.get('/', httpGetUsers)

module.exports = usersRouter
