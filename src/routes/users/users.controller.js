const { NotFoundError } = require('../../errors')
const { User } = require('../../models')

async function httpGetAuthUserHistory(req, res) {
    const { base, out, numericFilters } = req.query

    const user = await User.findById(req.user.userId)

    if (!user) {
        throw new NotFoundError('User not found')
    }

    let conversions = user.conversions

    if (base) {
        conversions = conversions.filter(c => c.base === base)
    }

    if (out) {
        conversions = conversions.filter(c => c.out === out)
    }

    if (numericFilters) {
        const operatorMap = {
            '>': (inp, filter) => inp > filter,
            '>=': (inp, filter) => inp >= filter,
            '<': (inp, filter) => inp < filter,
            '<=': (inp, filter) => inp <= filter,
            '=': (inp, filter) => inp === filter,
        }

        const regex = /\b(<|>|<=|>=|=)\b/g
        let filters = numericFilters.replace(regex, (match) => `-${match}-`)

        filters.split(',').forEach(item => {
            const [field, operator, value] = item.split('-')
            if (field === 'amount') {
                conversions = conversions.filter(c => operatorMap[operator](c.amount, value))
            } else if (field === 'convertedAmount') {
                conversions = conversions.filter(c => operatorMap[operator](c.convertedAmount, value))
            }
        })
    }

    res.json({
        success: true,
        data: {
            conversions,
        }
    })
}

async function httpGetUsers (req, res) {
    const users = await User.aggregate([
        { $match: {} },
        { $project: { name: '$name', conversions: { $size: '$conversions' } } },
    ])

    res.json({
        success: true,
        data: {
            users,
        }
    })
}

module.exports = {
    httpGetAuthUserHistory,
    httpGetUsers,
}